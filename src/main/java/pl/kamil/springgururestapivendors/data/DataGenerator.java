package pl.kamil.springgururestapivendors.data;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import pl.kamil.springgururestapivendors.domain.Vendor;
import pl.kamil.springgururestapivendors.repository.VendorRepository;

@Component
public class DataGenerator implements CommandLineRunner {

    private VendorRepository vendorRepository;

    public DataGenerator(VendorRepository vendorRepository) {
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        generateH2VendorData();
    }

    private void generateH2VendorData(){
        Vendor biedronka = new Vendor();
        biedronka.setName("Biedronka");

        Vendor lidl = new Vendor();
        lidl.setName("Lidl");

        Vendor zabka = new Vendor();
        zabka.setName("Zabka");

        Vendor dino = new Vendor();
        dino.setName("Dino");

        vendorRepository.save(biedronka);
        vendorRepository.save(lidl);
        vendorRepository.save(zabka);
        vendorRepository.save(dino);

        System.out.printf("Data added: %s", vendorRepository.count());
    }
}
