package pl.kamil.springgururestapivendors.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.kamil.springgururestapivendors.domain.Vendor;

public interface VendorRepository extends JpaRepository<Vendor, Long> {
}
