package pl.kamil.springgururestapivendors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringGuruRestApiVendorsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringGuruRestApiVendorsApplication.class, args);
    }
}
