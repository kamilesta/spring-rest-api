package pl.kamil.springgururestapivendors.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.kamil.springgururestapivendors.api.v1.model.VendorDTO;
import pl.kamil.springgururestapivendors.api.v1.model.VendorDTOList;
import pl.kamil.springgururestapivendors.repository.VendorRepository;
import pl.kamil.springgururestapivendors.services.VendorService;

import java.util.List;

import static pl.kamil.springgururestapivendors.controllers.VendorController.BASE_URL;

@RestController
@RequestMapping(BASE_URL)
public class VendorController {

    public static final String BASE_URL = "/api/v1/vendors";

    private VendorService vendorService;
    private VendorRepository vendorRepository;

    @Autowired
    public VendorController(VendorService vendorService, VendorRepository vendorRepository) {
        this.vendorService = vendorService;
        this.vendorRepository = vendorRepository;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public VendorDTOList getAllVendors(){
        return new VendorDTOList(vendorService.getVendors());
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public VendorDTO getVendorById(@PathVariable Long id){
        return vendorService.getVendorById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public VendorDTO createVendor(@RequestBody VendorDTO vendorDTO){
        return vendorService.createVendor(vendorDTO);
    }
}

