package pl.kamil.springgururestapivendors.services;

import org.springframework.stereotype.Service;
import pl.kamil.springgururestapivendors.api.v1.model.VendorDTO;
import pl.kamil.springgururestapivendors.domain.Vendor;
import pl.kamil.springgururestapivendors.mappers.VendorMapper;
import pl.kamil.springgururestapivendors.repository.VendorRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static pl.kamil.springgururestapivendors.controllers.VendorController.BASE_URL;

@Service
public class VendorServiceImpl implements VendorService {

    private VendorRepository vendorRepository;
    private VendorMapper mapper;

    public VendorServiceImpl(VendorRepository vendorRepository) {
        this.vendorRepository = vendorRepository;
        mapper = new VendorMapper();
    }

    @Override
    public List<VendorDTO> getVendors() {
        return vendorRepository.findAll()
                .stream()
                .map(vendor -> {
                    VendorDTO vendorDTO = mapper.vendorToVendorDto(vendor);
                    vendorDTO.setVendorUrl(BASE_URL + "/" + vendor.getId());
                    return vendorDTO;
                })
                .collect(Collectors.toList());
    }

    @Override
    public VendorDTO getVendorById(Long id) {
        return vendorRepository.findById(id)
                .map(vendor -> {
                    VendorDTO vendorDTO = mapper.vendorToVendorDto(vendor);
                    vendorDTO.setVendorUrl(BASE_URL + "/" + vendor.getId());
                    return vendorDTO;
                })
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public VendorDTO createVendor(VendorDTO vendorDTO) {
        Vendor returnedVendor = vendorRepository.save(mapper.vendorDtoToVendor(vendorDTO));
        return Optional.of(returnedVendor)
                .map(vendor -> {
                    VendorDTO returnedVendorDTO = mapper.vendorToVendorDto(vendor);
                    returnedVendorDTO.setVendorUrl(BASE_URL + "/" + returnedVendor.getId());
                    return returnedVendorDTO;
                })
                .orElseThrow(RuntimeException::new);
    }
}
