package pl.kamil.springgururestapivendors.services;

import pl.kamil.springgururestapivendors.api.v1.model.VendorDTO;
import pl.kamil.springgururestapivendors.domain.Vendor;

import java.util.List;

public interface VendorService {

    List<VendorDTO> getVendors();

    VendorDTO getVendorById(Long id);

    VendorDTO createVendor(VendorDTO vendor);
}
