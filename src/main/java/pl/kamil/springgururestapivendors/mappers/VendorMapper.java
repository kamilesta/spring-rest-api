package pl.kamil.springgururestapivendors.mappers;

import pl.kamil.springgururestapivendors.api.v1.model.VendorDTO;
import pl.kamil.springgururestapivendors.domain.Vendor;

public class VendorMapper {
    public VendorDTO vendorToVendorDto(Vendor vendor){
        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setId(vendor.getId());
        vendorDTO.setName(vendor.getName());
        return vendorDTO;
    }

    public Vendor vendorDtoToVendor(VendorDTO vendorDTO){
        Vendor vendor = new Vendor();
        vendor.setId(vendorDTO.getId());
        vendor.setName(vendorDTO.getName());
        return vendor;
    }
}
