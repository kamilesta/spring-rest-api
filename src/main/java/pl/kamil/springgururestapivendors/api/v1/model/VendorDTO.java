package pl.kamil.springgururestapivendors.api.v1.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class VendorDTO {
    private Long id;
    private String name;
    private String vendorUrl;
}
