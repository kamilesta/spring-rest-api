package pl.kamil.springgururestapivendors.controllers;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.verification.api.VerificationDataInOrder;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kamil.springgururestapivendors.TestUtils;
import pl.kamil.springgururestapivendors.api.v1.model.VendorDTO;
import pl.kamil.springgururestapivendors.api.v1.model.VendorDTOList;
import pl.kamil.springgururestapivendors.domain.Vendor;
import pl.kamil.springgururestapivendors.repository.VendorRepository;
import pl.kamil.springgururestapivendors.services.VendorService;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.kamil.springgururestapivendors.controllers.VendorController.BASE_URL;

@RunWith(MockitoJUnitRunner.class)
public class VendorControllerTest {

    private VendorController vendorController;

    @Mock
    private VendorRepository vendorRepository;

    @Mock
    private VendorService vendorService;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        vendorController = new VendorController(vendorService, vendorRepository);
        mockMvc = MockMvcBuilders.standaloneSetup(vendorController).build();
    }

    @Test
    public void shouldGetAllVendors() throws Exception {
        //given
        VendorDTO vendor1 = new VendorDTO();
        vendor1.setId(1L);
        vendor1.setName("name1");

        VendorDTO vendor2 = new VendorDTO();
        vendor2.setId(2L);
        vendor2.setName("name2");

        List<VendorDTO> vendors = Arrays.asList(vendor1, vendor2);

        //when
        when(vendorService.getVendors()).thenReturn(vendors);

        //then
        mockMvc.perform(get(BASE_URL).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.vendors", hasSize(2)));
    }

    @Test
    public void shouldGetVendorById() throws Exception {
        //given
        Long id = 1L;
        String name = "name";
        String url = BASE_URL + "/" + id;

        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setId(id);
        vendorDTO.setName(name);
        vendorDTO.setVendorUrl(url);

        //when
        when(vendorService.getVendorById(anyLong())).thenReturn(vendorDTO);

        //then
        mockMvc.perform(get(url).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(id.intValue())))
                .andExpect(jsonPath("$.name", equalTo(name)))
                .andExpect(jsonPath("$.vendorUrl", equalTo(url)));
    }

    @Test
    public void shouldCreateVendor() throws Exception {
        //given
        Long id = 1L;
        String name = "name";
        String url = BASE_URL + "/" + id;

        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setId(id);
        vendorDTO.setName(name);
        vendorDTO.setVendorUrl(url);

        //when
        when(vendorService.createVendor(any(VendorDTO.class))).thenReturn(vendorDTO);

        //then
        mockMvc.perform(post(url).contentType(MediaType.APPLICATION_JSON)
                .content(TestUtils.toJsonString(vendorDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", equalTo(id.intValue())))
                .andExpect(jsonPath("$.name", equalTo(name)))
                .andExpect(jsonPath("$vendorUrl", equalTo(url)));
    }
}