package pl.kamil.springgururestapivendors.services;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kamil.springgururestapivendors.api.v1.model.VendorDTO;
import pl.kamil.springgururestapivendors.controllers.VendorController;
import pl.kamil.springgururestapivendors.domain.Vendor;
import pl.kamil.springgururestapivendors.mappers.VendorMapper;
import pl.kamil.springgururestapivendors.repository.VendorRepository;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VendorServiceImplTest {

    private VendorServiceImpl vendorService;

    private VendorMapper mapper;

    @Mock
    private VendorRepository vendorRepository;

    @Before
    public void setUp() throws Exception {
        vendorService = new VendorServiceImpl(vendorRepository);
        mapper = new VendorMapper();
    }

    @Test
    public void shouldGetAllVendors() {
        //given
        Vendor vendor1 = new Vendor();
        vendor1.setName("Vendor1");

        Vendor vendor2 = new Vendor();
        vendor2.setName("Vendor2");

        List<Vendor> vendors = Arrays.asList(vendor1, vendor2);

        //when
        when(vendorRepository.findAll()).thenReturn(vendors);
        List<VendorDTO> returnedVendors = vendorService.getVendors();

        //then
        assertEquals(returnedVendors.size(), 2);

    }

    @Test
    public void shouldGetVendorById() {
        //given
        Vendor vendor =  new Vendor();
        vendor.setId(1L);
        vendor.setName("Name");

        Optional<Vendor> vendorOptional  = Optional.of(vendor);

        //when
        when(vendorRepository.findById(anyLong())).thenReturn(vendorOptional);
        VendorDTO returnedVendorDTO = vendorService.getVendorById(1L);

        //then
        assertEquals(vendor.getId(), returnedVendorDTO.getId());
        assertEquals(vendor.getName(), returnedVendorDTO.getName());
        assertEquals(VendorController.BASE_URL + "/" + vendor.getId(), returnedVendorDTO.getVendorUrl());
    }

    @Test
    public void shouldCreateVendor() {
        //given
        Vendor vendor = new Vendor();
        vendor.setId(1L);
        vendor.setName("Name");

        //when
        when(vendorRepository.save(any(Vendor.class))).thenReturn(vendor);
        VendorDTO returnedVendorDTO = vendorService.createVendor(mapper.vendorToVendorDto(vendor));

        //then
        assertEquals(vendor.getId(), returnedVendorDTO.getId());
        assertEquals(vendor.getName(), returnedVendorDTO.getName());
        assertEquals(VendorController.BASE_URL + "/" + vendor.getId(), returnedVendorDTO.getVendorUrl());
    }
}