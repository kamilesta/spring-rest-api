package pl.kamil.springgururestapivendors.mappers;

import org.junit.Before;
import org.junit.Test;
import pl.kamil.springgururestapivendors.api.v1.model.VendorDTO;
import pl.kamil.springgururestapivendors.domain.Vendor;
import pl.kamil.springgururestapivendors.repository.VendorRepository;

import static org.junit.Assert.*;

public class VendorMapperTest {

    private VendorMapper mapper;

    @Before
    public void setUp(){
        mapper = new VendorMapper();
    }

    @Test
    public void shouldMapVendorToVendorDto() {
        //given
        Vendor vendor = new Vendor();
        vendor.setId(1L);
        vendor.setName("Name");

        //when
        VendorDTO vendorDTO = mapper.vendorToVendorDto(vendor);

        //then
        assertEquals(vendor.getId(), vendorDTO.getId());
        assertEquals(vendor.getName(), vendorDTO.getName());
    }

    @Test
    public void vendorDtoToVendor() {
        //given
        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setId(1L);
        vendorDTO.setName("Name");

        //when
        Vendor vendor = mapper.vendorDtoToVendor(vendorDTO);

        //then
        assertEquals(vendorDTO.getId(), vendorDTO.getId());
        assertEquals(vendorDTO.getName(), vendorDTO.getName());
    }
}