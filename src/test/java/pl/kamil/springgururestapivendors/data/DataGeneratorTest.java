package pl.kamil.springgururestapivendors.data;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import pl.kamil.springgururestapivendors.domain.Vendor;
import pl.kamil.springgururestapivendors.repository.VendorRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DataGeneratorTest {

    @Mock
    private VendorRepository vendorRepository;

    private DataGenerator dataGenerator;

    @Before
    public void setUp() throws Exception {
        dataGenerator = new DataGenerator(vendorRepository);
    }

    @Test
    public void shouldGenerateH2Data() throws Exception {
        int numberOfEntriesGenerated = 4;
        dataGenerator.run();

        verify(vendorRepository, times(numberOfEntriesGenerated)).save(any(Vendor.class));
    }
}